#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Build sqlite3 PubTator DB.
Data:
- Text and annotations: https://ftp.ncbi.nlm.nih.gov/pub/lu/PubTatorCentral/bioconcepts2pubtatorcentral.offset.gz
- PMC <> PMID : https://ftp.ncbi.nlm.nih.gov/pub/pmc/PMC-ids.csv.gz
"""


import argparse
import sqlite3

import pandas as pd
from bioc import pubtator
from loguru import logger
from smart_open import smart_open
from tqdm import tqdm


def parse_args():
    """
    Parse arguments
    """
    parser = argparse.ArgumentParser(description="Build Pubtator sqlite database.")
    parser.add_argument(
        "--pubtator",
        help="`bioconcepts2pubtatorcentral.offset.gz` file in pubtator format",
        required=True,
        type=str,
    )
    parser.add_argument(
        "--pmcid_pmid",
        help="`PMC-ids.csv.gz` file with `PMCID -> PMID` mapping",
        required=True,
        type=str,
    )
    parser.add_argument(
        "--skip_pubtator", action="store_true", help="Skip `pubt_*` tables"
    )
    parser.add_argument(
        "--skip_pmcid_pmid", action="store_true", help="Skip `pmcid_pmid` table"
    )

    parser.add_argument(
        "--output", help="Output sqlite .db file.", required=True, type=str
    )
    parser.add_argument(
        "--overwrite", help="Recreate the database.", action="store_true"
    )
    return parser.parse_args()


def table_exists(table_name, connection):
    """
    Check if table exists
    """
    return (
        1
        in connection.execute(
            f"select count(*) from sqlite_master where type='table' and name='{table_name}'"
        ).fetchone()
    )


def create_articles_table(connection: sqlite3.Connection):
    """Create table with articles title and abstract"""
    connection.executescript(
        """
    DROP TABLE IF EXISTS "pubt_articles";
    CREATE TABLE pubt_articles(
    pmid INTEGER NOT NULL PRIMARY KEY,
    title TEXT NOT NULL,
    abstract TEXT DEFAULT NULL
    );
    """
    )


def create_annotation_table(entity_type: str, connection: sqlite3.Connection):
    """
    Table containing annotations
    """
    connection.executescript(
        f"""
    DROP TABLE IF EXISTS "pubt_{entity_type}";
    CREATE TABLE pubt_{entity_type}(
        pmid INTEGER NOT NULL REFERENCES pubt_passages(pmid),
        type TEXT NOT NULL,
        text TEXT NOT NULL,
        start INTEGER NOT NULL,
        end INTEGER NOT NULL
    )
    """
    )


def flush_annotations(annotations: dict, connection: sqlite3.Connection):
    """
    Insert annotations data into DB
    """
    for m_type, rows in annotations.items():
        if rows:
            connection.executemany(
                f"INSERT INTO pubt_{m_type} VALUES (?, ?, ?, ?, ?)", rows
            )
            rows.clear()


def flush_title_abstract(connection: sqlite3.Connection, doc: pubtator.PubTator):
    """
    Insert PubMed ID text into DB
    """

    connection.execute(
        "INSERT INTO pubt_articles VALUES (?, ?, ?)",
        (doc.pmi, doc.title, doc.abstract),
    )


def create_pubtator_tables(
    path: str, connection: sqlite3.Connection, overwrite: bool = False
):
    """
    Create all table for PubTator DB:
        - articles : PMID, TEXT, TITLE
        - pubt_ENTITY: PMID, TYPE, TEXT, START, END
    """

    if table_exists("pubt_articles", connection) and not overwrite:
        raise RuntimeError(
            "Table `pubt_articles` exists but overwrite was not passed! "
        )

    logger.info("Start creating PubTator tables...")
    create_articles_table(connection=connection)

    mentions_read = 0
    chunk_size = 1000
    annotations: dict = {}
    variants = {"ProteinMutation", "DNAMutation", "SNP"}

    with smart_open(path) as pubtator_file:

        progressbar = tqdm(desc="Loading articles", unit=" articles")

        for doc in pubtator.iterparse(pubtator_file):

            flush_title_abstract(doc=doc, connection=connection)

            for a in doc.annotations:

                a_type = a.type
                if a_type in variants:
                    a_type = "Variant"

                if a_type not in annotations:
                    annotations[a_type] = []
                    table_name = f"pubt_{a.type}"
                    if table_exists(table_name, connection) and not overwrite:
                        raise RuntimeError(
                            f"Table {table_name} text exists but overwrite was not passed! "
                        )
                    create_annotation_table(entity_type=a.type, connection=connection)

                annotations[a_type].append(
                    (
                        doc.pmid,
                        a.type,
                        a.text,
                        a.start,
                        a.end,
                    )
                )
                mentions_read += 1
            if mentions_read >= chunk_size:
                flush_annotations(annotations=annotations, connection=connection)
                mentions_read = 0
            progressbar.update(1)
    if mentions_read:
        flush_annotations(annotations=annotations, connection=connection)
    m_tables = [f"pubt_{m}" for m in annotations]
    connection.execute(
        f"CREATE VIEW pubt_mentions AS SELECT * FROM {', '.join(m_tables)};"
    )
    connection.commit()


def create_pmcid_pmid_table(
    path: str, connection: sqlite3.Connection, overwrite: bool = False
):
    """
    PMCID -> PMID
    """

    if table_exists("pmcid_pmid", connection) and not overwrite:
        raise RuntimeError("Table `pmcid_pmid` exists but overwrite was not passed! ")

    logger.info("Start creating `PMCID -> PMID` table...")
    connection.executescript(
        """
    DROP TABLE IF EXISTS "pmcid_pmid";
    CREATE TABLE pmcid_pmid(
    pmcid INTEGER NOT NULL PRIMARY KEY,
    pmid INTEGER NOT NULL
    );
    """
    )

    reader = pd.read_csv(path, chunksize=100000)

    for df in reader:
        df = df[["PMCID", "PMID"]].copy(deep=True)
        df.dropna(inplace=True)
        df["PMCID"] = df["PMCID"].apply(lambda x: x.replace("PMC", "")).astype(int)
        rows = list(zip(list(df["PMCID"]), list(df["PMID"])))
        try:
            connection.executemany("INSERT INTO pmcid_pmid VALUES (?, ?)", tuple(rows))
        except sqlite3.IntegrityError:
            logger.warning("Could not add PMCID -> PMID mapping for : {}", rows)

    connection.commit()


def main():
    """
    Run
    """

    args = parse_args()

    with sqlite3.connect(args.output) as connection:

        if not args.skip_pubtator:

            create_pubtator_tables(
                path=args.pubtator, connection=connection, overwrite=args.overwrite
            )

        if not args.skip_pmcid_pmid:

            create_pmcid_pmid_table(
                path=args.pmcid_pmid, connection=connection, overwrite=args.overwrite
            )


if __name__ == "__main__":
    main()
